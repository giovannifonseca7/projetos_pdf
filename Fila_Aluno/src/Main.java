
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Fila<Aluno> fila = new Fila<Aluno>();
		
		Aluno giovanni = new Aluno();
		Aluno renan = new Aluno();
		Aluno maria = new Aluno();
		
		giovanni.setNome("Giovanni");
		renan.setNome("Renan");
		maria.setNome("Maria");
		
		fila.insere(giovanni);
		fila.insere(renan);
		
		System.out.println(fila.vazia());
		
		System.out.println(fila.fila.size());
		
		System.out.println(fila.fila.contains(maria));
		
		fila.insere(maria);
		
		System.out.println(fila.fila.contains(maria));
		
		fila.remove();
		fila.remove();
		
		System.out.println(fila.fila.size());
		
		fila.remove();
		
		System.out.println(fila.vazia());
		
		

	}

}
