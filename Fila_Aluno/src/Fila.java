import java.util.LinkedList;

public class Fila<T> {
	
	LinkedList<T> fila = new LinkedList<T>();
	
	void insere(T t) {
		
		this.fila.add(t);
	}
	
	public T remove() {
		
		 return this.fila.remove(0);
	}
	
	boolean vazia() {
		
		return fila.isEmpty();
	}
	
	/*public String toString() {
		
		StringBuilder texto = new StringBuilder();
		
		texto.append("[");
		
		for (int i = 0; i < fila.size() - 1; i++) {
			
			texto.append(fila.get(i));
			texto.append(", ");
			i++;
			
		}
		
		texto.append(fila.get(fila.size()));
		texto.append("]");
		
		return texto.toString();
		
		
	}*/

}
