package ed;


public class Vetor {

	private Object[] objetos = new Object[2];
	int totalDeObjetos = 0, cont = 0;

	public void garanteEspaco() {

		if (this.totalDeObjetos == objetos.length) {

			Object[] novaArray = new Object[this.objetos.length * 2];

			for (int i = 0; i < this.objetos.length; i++) {

				novaArray[i] = objetos[i];
			}

			objetos = novaArray;
		}

	}

	public void adiciona(Object object) {
		//this.garanteEspaco();
		this.objetos[this.totalDeObjetos] = object;
		this.totalDeObjetos++;
	}

	public void adiciona(int posicao, Aluno aluno) {
		//this.garanteEspaco();
		if (!this.posicaoValida(posicao)) {
			throw new IllegalArgumentException("Posição inválida");
		}
		for (int i = this.totalDeObjetos - 1; i >= posicao; i -= 1) {
			this.objetos[i + 1] = this.objetos[i];
		}
		this.objetos[posicao] = aluno;
		this.totalDeObjetos++;
	}

	public Object pega(int posicao) {

		if (objetos[posicao] != null) {

			return objetos[posicao];
		}

		return null;
	}

	public void remove(int posicao) {
		if (!this.posicaoOcupada(posicao)) {
			throw new IllegalArgumentException("Posição inválida");
		}
		for (int i = posicao; i < this.totalDeObjetos - 1; i++) {
			this.objetos[i] = this.objetos[i + 1];
		}
		this.totalDeObjetos--;
	}

	public boolean contem(Aluno aluno) {

		for (int i = 0; i < totalDeObjetos; i++) {

			if (objetos[i].equals(aluno)) {

				return true;
			}

		}

		return false;
	}

	public int tamanho() {

		return totalDeObjetos;

	}

	public String toString() {

		if (totalDeObjetos == 0) {

			return ("[]");
		}
		StringBuilder builder = new StringBuilder();
		builder.append("[");

		for (int i = 0; i < this.totalDeObjetos - 1; i++) {

			builder.append(this.objetos[i]);
			builder.append(",");
		}

		builder.append(this.objetos[totalDeObjetos - 1]);
		builder.append("]");

		return builder.toString();

	}

	private boolean posicaoOcupada(int posicao) {
		return posicao >= 0 && posicao < this.totalDeObjetos;
	}

	private boolean posicaoValida(int posicao) {
		return posicao >= 0 && posicao <= this.totalDeObjetos;
	}

}
