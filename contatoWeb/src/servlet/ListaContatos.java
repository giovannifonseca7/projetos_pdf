package servlet;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import contato.Contato;

public class ListaContatos {

	ArrayList<Contato> lista = new ArrayList<Contato>();
	Contato contato;

	public void adiciona(Contato contato) {

		lista.add(contato);

	}

	/*
	 * public void getLista() {
	 * 
	 * for(int i = 0; i < lista.size(); i++) {
	 * 
	 * Contato provisorio = lista.get(i);
	 * 
	 * System.out.println("Nome: " + provisorio.getNome());
	 * System.out.println("Email: " + provisorio.getEmail());
	 * System.out.println("Endereço: " + provisorio.getEndereco()); } }
	 */

	public ArrayList<Contato> getLista() {

		return lista;

	}

}
