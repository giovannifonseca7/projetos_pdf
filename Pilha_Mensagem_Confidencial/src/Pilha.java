import java.sql.Wrapper;
import java.util.LinkedList;

public class Pilha<T> {
	
	private LinkedList <Character> pecas = new LinkedList<Character>();
	
	public void insere(Character t) {
		
		this.pecas.add(t);
	}
	
	public Object remove() {
		
		return pecas.remove(this.pecas.size() - 1);
	}
	
	public boolean vazia() {
		
		//return pecas.size() == 0;
		
		//ou
		
		return pecas.isEmpty();
	}
	
	

}
