import java.awt.List;
import java.util.ArrayList;
import java.util.LinkedList;

public class ConjuntoEspalhamento {

	private ArrayList<LinkedList<String>> tabela = new ArrayList<LinkedList<String>>();
	// private List<List<String>> tabela = new ArrayList<List<String>>();

	int tamanho = 0;

	public ConjuntoEspalhamento() {
		for (int i = 0; i < 26; i++) {
			LinkedList<String> lista = new LinkedList<String>();
			tabela.add(lista);
		}
	}

	public void adiciona(String palavra) {

		if (!contem(palavra)) {

			int indice = calculaIndiceDaTabela(palavra);
			LinkedList<String> lista = tabela.get(indice);
			lista.add(palavra);
			tamanho++;
		}

	}

	public void remove(String palavra) {

		if (this.contem(palavra)) {

			int indice = calculaIndiceDaTabela(palavra);
			LinkedList<String> lista = tabela.get(indice);
			lista.remove(palavra);
			tamanho--;
		}

	}

	public boolean contem(String palavra) {
		int indice = this.calculaIndiceDaTabela(palavra);
		LinkedList<String> lista = this.tabela.get(indice);
		return lista.contains(palavra);
	}

	public ArrayList<String> pegaTodas() {
		ArrayList<String> palavras = new ArrayList<String>();

		for (int i = 0; i < this.tabela.size(); i++) {

			palavras.addAll(this.tabela.get(i));
		}
		return palavras;
	}

	public int tamanho() {

		return tamanho;

	}

	private int calculaIndiceDaTabela(String palavra) {
		int codigoDeEspalhamento = this.calculaCodigoDeEspalhamento(palavra);
		codigoDeEspalhamento = Math.abs(codigoDeEspalhamento);
		return codigoDeEspalhamento % tabela.size();
	}

	private int calculaCodigoDeEspalhamento(String palavra) {
		int codigo = 1;
		for (int i = 0; i < palavra.length(); i++) {
			codigo = 31 * codigo + palavra.charAt(i);
		}
		return codigo;
	}

	public void imprimeTabela() {
		for (LinkedList<String> lista : this.tabela) {

			System.out.print("[");
			for (int i = 0; i < lista.size(); i++) {
				System.out.print("*");
			}
			System.out.println("]");
		}
	}

	private void redimensionaTabela(int novaCapacidade) {
		ArrayList<String> palavras = this.pegaTodas();
		this.tabela.clear();
		for (int i = 0; i < novaCapacidade; i++) {
			this.tabela.add(new LinkedList<String>());
		}
		for (String palavra : palavras) {
			this.adiciona(palavra);
		}
	}

}
