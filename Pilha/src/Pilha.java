import java.awt.List;
import java.util.LinkedList;

public class Pilha<T> {
	
	private LinkedList <T> pecas = new LinkedList<T>();
	
	public void insere(T t) {
		
		this.pecas.add(t);
	}
	
	public Object remove() {
		
		return pecas.remove(this.pecas.size() - 1);
	}
	
	public boolean vazia() {
		
		//return pecas.size() == 0;
		
		//ou
		
		return pecas.isEmpty();
	}
	
	

}
