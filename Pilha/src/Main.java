import java.util.LinkedList;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Pilha<Peca> pilha = new Pilha<Peca>();

		Peca p1 = new Peca();

		pilha.insere(p1);

		/* Peca pecaRemove = (Peca) pilha.remove(); */

		/*
		 * if(pilha.vazia()) {
		 * 
		 * System.out.println("Pilha vazia"); }
		 */

		Pilha<String> pilha2 = new Pilha<String>();

		pilha2.insere("Maria");
		pilha2.insere("Adalberto");

		String maria = (String) pilha2.remove();
		String adalberto = (String) pilha2.remove();

		System.out.println("Maria: " + maria);
		System.out.println("Adalberto:" + adalberto);

		if (pilha2.vazia()) {

			System.out.println("Pilha 2 vazia");
		}

	}

}
