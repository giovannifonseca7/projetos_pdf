
public class Main2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Pilha<Peca> pilha = new Pilha<Peca>();
		
		Peca peca = new Peca();
		
		pilha.insere(peca);
		
		Peca pecaRemovida = (Peca) pilha.remove();
		
		if (peca != pecaRemovida) {
			System.out.println("Erro: a peça removida não é igual " + " a que foi inserida");

		}
		if (!pilha.vazia()) {
			System.out.println("Erro: A pilha não está vazia");
		}

	}

}
