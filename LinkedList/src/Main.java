import java.util.LinkedList;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Aluno maria = new Aluno();
		maria.setNome("Maria");
		
		Aluno joaquim = new Aluno();
		joaquim.setNome("Joaquim");
		
		Aluno giovanni = new Aluno();
		giovanni.setNome("Giovanni");
		
		LinkedList<Aluno> listaLigada = new LinkedList<>();
		
		listaLigada.add(maria);
		listaLigada.addFirst(joaquim);
		listaLigada.add(1, giovanni);
		listaLigada.addLast(maria);
		
		System.out.println(listaLigada);

	}

}
